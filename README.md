urp4yzu
=========

```            
                     __ __
                    /\ \\ \
__  __  _   __   _____\ \ \\ \    __  __  ____    __  __
/\ \/\ \/\`'__\/\ '__`\ \ \\ \_ /\ \/\ \/\_ ,`\ /\ \/\ \
\ \ \_\ \ \ \/ \ \ \L\ \ \__ ,__\ \ \_\ \/_/  /_\ \ \_\ \
 \ \____/\ \_\  \ \ ,__/\/_/\_\_/\/`____ \/\____\\ \____/
  \/___/  \/_/   \ \ \/    \/_/   `/___/> \/____/ \/___/
                  \ \_\              /\___/
                   \/_/              \/__/
                    ... 分为api_server与web_server两个部分
```
咱扬大妹纸揍是棒棒哒

##### 提示：我只是编写了此项目代码，并不会去部署。那么谁去部署呢？就看谁肯当一次雷锋了，么么哒
依赖
-----------

需要 Python 2.7 和以下模块:

* flask
* requests
* pyquery
* gunicorn

#### web_server
- 使用flask框架编写的web服务
- 使用requests通过http方式与api_server通信
- 你需要修改app.py中apiserver为你运行的api_server的地址
- 三个路由分别对应三个页面：登录页，介绍页，成绩页

#### api_server
- 同样是flask框架
- 数据库使用sqlite3，存取uuid对应的session
- 正常情况下，比如你登录失败或者显示了你的成绩后数据库中的session会删除
- 但是如果你只是刷新下验证码，你的uuid将分配到新的一个，旧的会依然存储在sqlite3数据库，但这无法造成你的登录信息可以被我获取，请放心。
- api_server有3条路由，分别对应获得uuid与urp的验证码，尝试登录urp，拉取本学期成绩成绩
- dbplay.py中是对应的增删查改的method
- urp.py 中是核心的模拟登录及将本学期成绩数据提取并转成json格式

#### 前端模板
- [AdminBSBMaterialDesign](https://github.com/gurayyarar/AdminBSBMaterialDesign "AdminBSBMaterialDesign") ，所以当前这个项目也用MIT LICENSE

#### 版权声明和许可声明
- MIT License
