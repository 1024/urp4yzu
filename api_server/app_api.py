#!/usr/bin/env python
# -*- coding: utf-8 -*-

from flask import Flask, request, Response
import requests, json
import uuid
import urp, dbplay

try:
    dbplay.createDB()
except:
    pass

app = Flask( __name__ )
app.secret_key = '\x8bY\xf9\x8a\x81bW\xa2\x86y\x0eQ\xbf\xb1\x8c\xe7\x9aK\x0f\xe8\xe5\xd95_'

@app.route( '/api/uc',  methods=["GET"] )
def ucapture():
    userid = uuid.uuid1().hex
    s = urp.getSession()
    s, b64img = urp.getCaptcha(s)
    dbplay.insertDB( userid, s)
    return Response( json.dumps({"userid": userid, "captcha": b64img}) )

@app.route( '/api/login',  methods=["GET"] )
def login():
    userid = request.args.get("uuid", "")
    captcha = request.args.get("captcha", "")
    username = request.args.get("username", "")
    password = request.args.get("password", "")
    s = dbplay.returnSession(userid)
    s, result = urp.getLogin( s, captcha, username, password )
    if result == "ok":
        dbplay.updateDB( userid, s)
    else:
        s.close()
        dbplay.delSession( userid )
    return Response( json.dumps({"result": result}) )

@app.route( '/api/s',  methods=["GET"] )
def score():
    userid = request.args.get("uuid", "")
    s = dbplay.returnSession(userid)
    bxqcj = urp.getBxqcj(s)
    s.close()
    dbplay.delSession( userid )
    return Response( bxqcj )

if __name__ == '__main__':
    app.run( host="127.0.0.1", port=12312, debug=False )
