#! /usr/bin/env python
# -*- coding: utf-8 -*-

import cPickle
import sqlite3
import os

ROOT = os.path.dirname(os.path.abspath(__file__))

def getDB():
  conn = sqlite3.connect(ROOT + os.sep + "session.db")
  return conn

def createDB():
  conn = getDB()
  cur = conn.execute("CREATE TABLE isession ( uuid varchar(64), reqsession BLOB)")
  conn.commit()

def insertDB( userid, s ):
  conn = getDB()
  cu = conn.cursor()
  ps = cPickle.dumps(s, cPickle.HIGHEST_PROTOCOL)
  cu.execute("insert into isession(uuid, reqsession) values (?,?)", (userid, sqlite3.Binary(ps)) )
  cu.close()
  conn.commit()

def updateDB( userid, s ):
  conn = getDB()
  cu = conn.cursor()
  ps = cPickle.dumps(s, cPickle.HIGHEST_PROTOCOL)
  cu.execute("UPDATE  isession SET reqsession=? where uuid=?", (sqlite3.Binary(ps), userid) )
  cu.close()
  conn.commit()

def returnSession( userid ):
  conn = getDB()
  cu = conn.cursor()
  cc = cu.execute("select * from isession where uuid =?", (userid,))
  hisc = cc.fetchall()[-1]
  s = cPickle.loads(str(hisc[1]))
  cu.close()
  return s

def delSession( userid ):
  conn = getDB()
  cu = conn.cursor()
  cu.execute("DELETE FROM isession WHERE uuid = ?", (userid,))
  cu.close()
  conn.commit()
