#! /usr/bin/env python
# -*- coding: utf-8 -*-

import json
import re
from pyquery import PyQuery
import requests
from base64 import encodestring as es
host = ''# urp地址，比如jw2.yzu.edu.cn

def getSession():
  session = requests.Session()
  session.headers["Host"] = host
  session.headers["User-Agent"] = "Mozilla/5.0 (Windows NT 6.3; Trident/7.0; rv:11.0) like Gecko"
  session.headers["Origin"] = "http://"+host
  #session.proxies = {"http": "socks5://127.0.0.1:2080", "https": "socks5://127.0.0.1:2080"}# 你可能需要代理
  return session

def getCaptcha( session ):
  vcode = session.get( 'http://' + host + '/validateCodeAction.do', timeout=10)
  return session, es(vcode.content).rstrip()

def getLogin( session, captcha, username, password ):
  loginsio = session.post('http://' + host + '/loginAction.do', 
    data = { 'zjh': username,
             'mm': password,
             'v_yzm': captcha
           }, timeout=10)
  rl = loginsio.content.decode("gbk").encode("utf-8")
  result = "ok"
  try:
    error = re.search("strong><font color.*?>(.*?)<", rl).group(1)
    if len(error) > 53 or 30 < len(error) < 48:
      result = "wp"
    elif len(error) < 29:
      result = "sw"
    else:
      result = "wy"
  except:
    pass
  return session, result

def getBxqcj( session ):
  bxqcj_html =  session.get("http://" + host + "/bxqcjcxAction.do").text
  session.get("http://" + host + "/logout.do")
  cj = extracted(bxqcj_html)
  return json.dumps(cj)

f=lambda a:map(lambda b:a[b:b+12],range(0,len(a)-1,12))
def extracted(bxqcj_html):
  x =  PyQuery(bxqcj_html)("thead > tr > td")
  kc_all = []
  for xx in x: kc_all.append(xx.text.strip())
  return f(kc_all)
