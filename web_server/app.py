#!/usr/bin/env python
# -*- coding: utf-8 -*-

from flask import Flask, render_template, session, request, redirect, Response
import json
import requests
from base64 import decodestring as de

apiserver = "http://127.0.0.1:12312"

app = Flask( __name__ )
app.secret_key = 'x\xa0)\x98\x9d\x91\x92S\x9b]\xb7\xd3hq\xe1\xb4\xf2\xc4\xed\x888\x12\xbcA'

@app.route( '/', methods=["GET", "POST"] )
def index():
    if request.method  == "POST":
        user = request.form.get("username", "")
        pwd = request.form.get("password", "")
        captcha = request.form.get("captcha", "")
        agree = request.form.get("agree", "")
        if agree != "on":
            return render_template( 'signin.htm', flag="w", msg=u"您未授权代您登录，程序已停止运行"  )
        jresult = json.loads(requests.get(apiserver + "/api/login?uuid=%s&captcha=%s&username=%s&password=%s"%(session.get("userid", ""), captcha, user, pwd) ).content)
        result = jresult["result"]
        fm = {"ok": 
                   {"flag": "s", "msg": u"密码正确，登录成功"}, 
              "wp": 
                   {"flag": "e", "msg": u"密码错误"},
              "sw": 
                   {"flag": "e", "msg": u"教务系统服务器认为你不太像人类，或者教务服务器运行异常"}, 
              "wy": 
                   {"flag": "e", "msg": u"验证码错误"}}
        if result == "ok":
            return redirect("/s")
        rm = fm[result]
        return render_template( 'signin.htm', flag=rm["flag"], msg=rm["msg"] ) 
    return render_template( 'signin.htm', flag="i", msg=u"么么哒" )

@app.route( '/a' )
def about():
    return render_template( 'about.htm' )

@app.route( '/s' )
def score():
    userid = session.get("userid", "")
    co = requests.get(apiserver + "/api/s?uuid=%s"%userid ).content
    cjs = json.loads(co)
    zs = len(cjs)
    cs = 0
    cjs2 = []
    for item in cjs:
        flag = ""
        if item[-3]:
            cs += 1
            if item[-3] > "84.5":
                flag = "success"
            elif item[-3] < "60":
                flag = "danger"
            else:
                flag = "warning"
        g = []
        g.append(flag)
        cjs2.append( item + g)
    bfs = 100*float(cs)/zs
    return render_template( 'score.htm', bfs=bfs, cs=cs, zs=zs, cjs=cjs2 )

@app.route( '/fonts/<string:font>' )
def mvfonts2static(font):
    return redirect( '/static/'+font )

@app.route( '/captcha' )
def giveyouacaptcha():
    uc = json.loads(requests.get(apiserver + "/api/uc" ).content)
    userid = uc["userid"]
    session["userid"] = userid
    print "userid", userid
    cimg = de( uc["captcha"] )
    return Response( cimg, mimetype='image/jpeg' )

@app.route( '/captcha.php' )
def captchaphp():
    return giveyouacaptcha()

if __name__ == '__main__':
    app.run( host="0.0.0.0", port=8000, debug=False )
